package com.mustafogli.eClinic.controller;

import com.mustafogli.eClinic.model.News;
import com.mustafogli.eClinic.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NewsController {
    @Autowired
    private NewsService newsService;


    @GetMapping("/addNews")
    public String addNews(Model model){
        News news = new News();
        model.addAttribute("news", news);
        return "new_news";
    }

    @PostMapping("/saveNews")
    public String saveNews(@ModelAttribute("news") News news){
        newsService.saveNews(news);
        return "redirect:/news";
    }

    @GetMapping("/updateNews/{id}")
    public String updateNews(@PathVariable(value = "id") long id, Model model){
        News news = newsService.getNewsById(id);

        model.addAttribute("news", news);
        return "update_news";

    }

    @GetMapping("/deleteNews/{id}")
    public String deleteNews(@PathVariable(value = "id") long id){
        this.newsService.deleteNewsById(id);
        return "redirect:/news";
    }
}
