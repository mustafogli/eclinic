package com.mustafogli.eClinic.controller;


import com.mustafogli.eClinic.model.Doctor;
import com.mustafogli.eClinic.service.DoctorService;
import com.mustafogli.eClinic.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DoctorController {
    @Autowired
    private NewsService newsService;

    @Autowired
    private DoctorService doctorService;

    @GetMapping("/admin")
    public String viewDoctorPage(Model model){
        model.addAttribute("listDoctors", doctorService.getAllDoctors());

        return "admin";
    }

    @GetMapping("/news")
    public String viewNewsPage(Model model){

        model.addAttribute("listNews", newsService.getAllNews());
        return "news";
    }

    @GetMapping("/showNewDoctorForm")
    public String showNewDoctorForm(Model model){
        Doctor doctor = new Doctor();
        model.addAttribute("doctor", doctor);
        return "new_doctor";
    }

    @PostMapping("/saveDoctor")
    public String saveDoctor(@ModelAttribute("doctor") Doctor doctor){
        doctorService.saveDoctor(doctor);
        return "redirect:/admin";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") long id, Model model){
        Doctor doctor = doctorService.getDoctorById(id);

        model.addAttribute("doctor", doctor);
        return "update_doctor";

    }

    @GetMapping("/deleteDoctor/{id}")
    public String deleteDoctor(@PathVariable(value = "id") long id){
        this.doctorService.deleteDoctorById(id);
        return "redirect:/admin";
    }
}
