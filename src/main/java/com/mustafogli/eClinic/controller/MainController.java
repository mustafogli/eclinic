package com.mustafogli.eClinic.controller;

import com.mustafogli.eClinic.model.Appoint;
import com.mustafogli.eClinic.repository.AppointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @Autowired
    private AppointRepository appointRepository;

    @PostMapping("/appoint")
    public String appoint(Appoint appoint){

        appointRepository.save(appoint);
        return "redirect:/";
    }

    @RequestMapping("/")
    public String indexPage(Model model){
        return "index";
    }

    @RequestMapping("/user")
    public String user(Model model){
        return "index";
    }

    @GetMapping("/news-detail")
    public String news(){
        return "news-detail";
    }

    @GetMapping("/register")
    public String signup() {
        return "register";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/about_doctor")
    public String doctor(){
        return "about_doctor";
    }
}
