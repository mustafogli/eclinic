package com.mustafogli.eClinic.service;

import com.mustafogli.eClinic.model.News;

import java.util.List;

public interface NewsService {
    List<News> getAllNews();
    void saveNews(News news);
    News getNewsById(long id);
    void deleteNewsById(long id);
}
