package com.mustafogli.eClinic.service;

import com.mustafogli.eClinic.model.News;
import com.mustafogli.eClinic.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NewsServiceImp implements NewsService {
    @Autowired
    private NewsRepository newsRepository;

    @Override
    public List<News> getAllNews() {
        return newsRepository.findAll();
    }

    @Override
    public void saveNews(News news)  {
        this.newsRepository.save(news);
    }

    @Override
    public News getNewsById(long id) {
        Optional<News> optional = newsRepository.findById(id);
        News news = null;
        if(optional.isPresent()){
            news=optional.get();
        }else{
            throw new RuntimeException("News not found for id ::"+id);
        }
        return news;
    }

    @Override
    public void deleteNewsById(long id) {
        this.newsRepository.deleteById(id);
    }

}
