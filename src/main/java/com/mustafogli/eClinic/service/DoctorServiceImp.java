package com.mustafogli.eClinic.service;

import com.mustafogli.eClinic.model.Doctor;
import com.mustafogli.eClinic.repository.DoctorRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DoctorServiceImp implements DoctorService {

    @Autowired
    private DoctorRepositoy doctorRepositoy;

    @Override
    public List<Doctor> getAllDoctors() {
        return doctorRepositoy.findAll();
    }

    @Override
    public void saveDoctor(Doctor doctor)  {
        this.doctorRepositoy.save(doctor);
    }

    @Override
    public Doctor getDoctorById(long id) {
        Optional<Doctor> optional = doctorRepositoy.findById(id);
        Doctor doctor = null;
        if(optional.isPresent()){
            doctor=optional.get();
        }else{
            throw new RuntimeException("Employee not found for id ::"+id);
        }
        return doctor;
    }

    @Override
    public void deleteDoctorById(long id) {
        this.doctorRepositoy.deleteById(id);
    }
}
