package com.mustafogli.eClinic.service;

import com.mustafogli.eClinic.model.Doctor;

import java.util.List;

public interface DoctorService {
    List<Doctor> getAllDoctors();
    void saveDoctor(Doctor doctor);
    Doctor getDoctorById(long id);
    void deleteDoctorById(long id);
}
