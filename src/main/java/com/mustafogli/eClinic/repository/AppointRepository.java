package com.mustafogli.eClinic.repository;

import com.mustafogli.eClinic.model.Appoint;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointRepository extends CrudRepository<Appoint,Integer> {
}
