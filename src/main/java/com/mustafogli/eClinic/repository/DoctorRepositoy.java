package com.mustafogli.eClinic.repository;

import com.mustafogli.eClinic.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepositoy extends JpaRepository<Doctor, Long> {

}
