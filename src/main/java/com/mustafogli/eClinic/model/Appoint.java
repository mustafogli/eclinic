package com.mustafogli.eClinic.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
@Getter
@Setter
public class Appoint {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(length = 100, nullable = false)
    private String fullname;
    @Email
    @Column(length = 64, nullable = false)
    private String email;
    @Column(length = 64, nullable = false)
    private String date;
    private Integer number;
    private String message;

    public Appoint() {
    }

    public Appoint(String fullname, @Email String email, String date, Integer number, String message) {
        this.fullname = fullname;
        this.email = email;
        this.date = date;
        this.number = number;
        this.message = message;
    }
}
