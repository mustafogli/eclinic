package com.mustafogli.eClinic.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Setter
@Getter
@Table(name = "news")
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @NotEmpty
    @Size(min =3, max=100)
    @Column(name = "title_news")
    private String titleNews;

    @NotNull
    @NotEmpty
    @Size(min =3, max=300)
    @Column(name = "content_news")
    private String contentNews;

    @NotNull
    @NotEmpty
    @Column(name = "date_news")
    private String dateNews;
}
