package com.mustafogli.eClinic.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@Table(name="doctors")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @NotEmpty
    @Size(min =3, max=25)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @NotEmpty
    @Size(min =3, max=25)
    @Column(name = "last_name")
    private String lastName;


    @NotNull
    @NotEmpty
    @Size(min =3, max=50)
    @Column(name = "position")
    private String position;

    @NotNull
    @NotEmpty
    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @NotNull
    @NotEmpty
    @Column(name="phone_number")
    private String phoneNumber;

    @NotNull
    @NotEmpty
    @Column(name = "passport_number")
    private String passportNumber;

}
